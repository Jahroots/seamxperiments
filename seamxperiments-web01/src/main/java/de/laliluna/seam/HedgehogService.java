package de.laliluna.seam;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.ScopeType;
import org.hibernate.Session;

import java.util.List;
import java.util.Date;
import java.io.Serializable;

/**
 * (c)2008 www.laliluna.de
 *
 * @author Sebastian Hennebrueder
 */

@Name("hedgehogService")
@Scope(ScopeType.CONVERSATION)
public class HedgehogService implements Serializable {

  @In
  private Session session;

  @DataModel
  private List<Hedgehog> hedgehogs;

  public HedgehogService() {
  }

  public String getHello() {
    return "Your hedgehogService says hello";
  }

  @Factory("hedgehogs")
  public void loadHedgehogs() {
    hedgehogs = session.createCriteria(Hedgehog.class).list();
  }

  public void createHedgehog() {
    session.save(new Hedgehog("Name " + new Date()));
  }
}
